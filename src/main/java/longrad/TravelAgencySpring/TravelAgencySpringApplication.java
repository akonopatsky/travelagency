package longrad.TravelAgencySpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelAgencySpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelAgencySpringApplication.class, args);
	}

}
